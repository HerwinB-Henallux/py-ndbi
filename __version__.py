VERSION = "1.0.0-lite"
"""Module's version"""

SCHEMA_VERSION = "0.0.0"
"""Should only be used if your schema will change in the future !"""
