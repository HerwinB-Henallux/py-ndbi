# Fixing the path to make testing easier
import os
import sys
new_path = [p for p in sys.path if not ("ndbi" in p)]
sys.path = [os.path.abspath(os.path.join(os.path.dirname(os.path.realpath(__file__)), "../"))] + new_path

from ndbi.interactor.sqlite import SQLiteInteractor
from ndbi import format_sql


def main():
    sqli = SQLiteInteractor("./test.sqlite")
    sqli.connect()
    sqli.update(format_sql("""CREATE TABLE IF NOT EXISTS `meta_db` (
  `{field_pk}` INTEGER UNIQUE PRIMARY KEY AUTOINCREMENT,
  `name` varchar(255) UNIQUE NOT NULL
  );"""))
    sqli.close()


if __name__ == '__main__':
    main()
