import sqlite3
from typing import Union
from . import IInteractor
from .. import QUERY_DICT


class SQLiteInteractor(IInteractor):
    _con: Union[sqlite3.Connection, None]
    
    def __init__(self, host: str, username: Union[str, None] = None, password: Union[str, None] = None):
        super(SQLiteInteractor, self).__init__(host, None, username, password)
    
    def _is_con_valid(self) -> bool:
        if self._con is None:
            return False
    
        if not (type(self._con) is sqlite3.Connection):
            raise ValueError("The internal '_con' object is neither a 'sqlite3.Connection' object or None !")
    
        return True
    
    def is_connected(self) -> bool:
        return self._is_con_valid()
    
    def connect(self, **kwargs) -> bool:
        self._con = sqlite3.connect(self.host)
        return self._is_con_valid()

    def close(self) -> bool:
        if self._is_con_valid():
            self._con.commit()
            self._con.close()
            self._con = None
        else:
            raise ValueError("The internal '_con' object is already closed or was never opened !")
        return True
    
    def update(self, query: str, do_resolve_standard_fields: bool = True) -> None:
        if not self._is_con_valid():
            raise ValueError("The internal '_con' object is already closed or was never opened !")
        
        if do_resolve_standard_fields:
            query = query.format_map(QUERY_DICT)
        
        cursor = self._con.cursor()
        try:
            print("$> "+query)
            cursor.executescript(query)
        except sqlite3.OperationalError as err:
            raise ConnectionError(err)
        finally:
            cursor.close()
            self._con.commit()
    
    def query(self, query: str, do_resolve_standard_fields: bool = True) -> list:
        if not self._is_con_valid():
            raise ValueError("The internal '_con' object is already closed or was never opened !")
        
        if do_resolve_standard_fields:
            query = query.format_map(QUERY_DICT)
        
        cursor = self._con.cursor()
        result = None
        
        try:
            print("$> "+query)
            cursor.execute(query)
            result = cursor.fetchall()
        except sqlite3.OperationalError as err:
            raise ConnectionError(err)
        finally:
            cursor.close()
        
        return result
