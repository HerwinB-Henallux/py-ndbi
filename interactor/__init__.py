from abc import ABC, abstractmethod
from typing import Union


class IInteractor(ABC):
    host: str
    port: Union[int, None]
    username: Union[str, None]
    password: Union[str, None]
    
    def __init__(self, host: str, port: Union[int, None] = None, username: Union[str, None] = None,
                 password: Union[str, None] = None):
        self.host = host
        self.port = port
        self.username = username
        self.password = password
    
    @abstractmethod
    def is_connected(self) -> bool:
        pass
    
    @abstractmethod
    def connect(self, **kwargs) -> bool:
        pass
    
    @abstractmethod
    def close(self) -> bool:
        pass
    
    @abstractmethod
    def update(self, query: str, do_resolve_standard_fields: bool = True):
        pass
    
    @abstractmethod
    def query(self, query: str, do_resolve_standard_fields: bool = True) -> list:
        pass
