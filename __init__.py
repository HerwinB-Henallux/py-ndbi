import random
from . import __version__

_HEX_ALPHABET = list("0123456789ABCDEF")
"""List of chars used to manually generate a hexadecimal number directly as a string"""

DB_FIELD_PK: str = "uid"
"""Default fields' name for the primary key."""


class SafeQueryDict(dict):
    def __missing__(self, key):
        return '{' + key + '}'


QUERY_DICT = SafeQueryDict()
QUERY_DICT["field_pk"] = DB_FIELD_PK
QUERY_DICT["data_version_default"] = __version__.SCHEMA_VERSION


# Utilities
def get_uid_as_str(prefix: bool = True) -> str:
    hex_number = ""
    for i in range(int(64 / 4)):
        hex_number += _HEX_ALPHABET[random.randint(0, len(_HEX_ALPHABET) - 1)]
    return ("0x" if prefix else "") + hex_number


def get_uid_as_int() -> int:
    # This was easier than making sure calls to random were working properly.
    return int(get_uid_as_str(), 16)


def format_sql(sql_query: str) -> str:
    return sql_query.format_map(QUERY_DICT)
