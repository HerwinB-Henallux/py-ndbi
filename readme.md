# Nibble DataBase Interactor
A simple a crude python module that aims to simplify some accesses to DB in Python.

This module is a trimmed-down version of my module used in the "content-indexer" project for the UPP.

## License
[Unlicense](LICENSE) <sub><sup>(Inherited from 'content-indexer')</sup></sub>
